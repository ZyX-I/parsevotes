#!/usr/bin/env python
'''Script for using ranked voting system
'''
from __future__ import unicode_literals, print_function, division

from github import Github

import sys
import re
from collections import defaultdict, Counter
from getpass import getpass


def get_credentials():
    '''Get user/password pair from script arguments

    Uses getpass if user was given, but no password'''
    try:
        user = sys.argv[2]
        try:
            password = sys.argv[3]
        except IndexError:
            password = getpass()
    except IndexError:
        user = None
        password = None
    return user, password


def parse_repository(repository):
    '''Parse repository argument

    Returns a pair with first value suitable for Github.get_repo and second for
    github.Repository.Repository.get_issue.
    '''
    repo, issue = repository.rpartition('#')[::2]
    issue = int(issue)
    return repo, issue


def get_issue(github, repo, issue):
    '''Returns github.Issue.Issue object.'''
    grepo = github.get_repo(repo)
    gissue = grepo.get_issue(issue)
    return gissue


def list_comments(gissue):
    '''List github comments yielding user/comment pair
    '''
    for gcomment in gissue.get_comments():
        yield gcomment.user.login, gcomment.body


def get_variants(gissue):
    '''Get variants voted on

    Assumes they are in a table:

        --------|
        variant1|
        var2    |
        ...
    '''
    table_start_re = re.compile(r'^\s*-+\|')
    variant_re = re.compile(r'^\s*(\S+)')

    variants = set()

    started = False
    for line in gissue.body.split('\n'):
        if started:
            match = variant_re.match(line)
            if match:
                variants.add(match.group(1))
            else:
                break
        else:
            started = bool(table_start_re.match(line))

    return variants


def get_votes(variants, gissue):
    '''Get user votes
    '''
    votes = defaultdict(int)

    voters = set()

    for login, comment in list_comments(gissue):
        for line in comment.split('\n'):
            user_votes = [vote.strip(' -\r') for vote in line.split('>')]
            if not user_votes:
                continue
            if not all((vote in variants for vote in user_votes)):
                if len(user_votes) != 1:
                    sys.stderr.write(
                        'Unknown votes from user {0}: {1!r}\n\n'.format(
                            login, user_votes))
                continue
            votes_set = set(user_votes)
            if votes_set - variants:
                sys.stderr.write('Unknown variants: {0!r}\n\n'.format(
                    votes_set - variants))
                continue
            sys.stderr.write('{0}: {1!r}\n'.format(login, user_votes))
            counter = Counter(user_votes)
            if counter.most_common()[0][1] > 1:
                sys.stderr.write('Duplicate items: {0!r}\n\n'.format(
                    [vote for vote, _ in counter.most_common()]))
                continue
            if login in voters:
                sys.stderr.write('User {0} already voted\n\n'.format(login))
                continue
            else:
                voters.add(login)
            sys.stderr.write('\n')
            for points, vote in enumerate(reversed(user_votes)):
                votes[vote] += points + (len(variants) - len(user_votes))

    return votes


def main():
    '''Main function'''
    if sys.argv[1] == '--help':
        print (__doc__)
        print ('Usage:')
        print ('    parsevotes.py author/repo#issue [user [password]]')
        sys.exit(1)

    try:
        repository = sys.argv[1]
    except IndexError:
        sys.stderr.write(
            'You must specify the author/repo#issue as a first argument\n'
        )
        sys.exit(2)
    repo, issue = parse_repository(repository)

    user, password = get_credentials()
    github = Github(user, password)

    gissue = get_issue(github, repo, issue)

    variants = get_variants(gissue)

    votes = get_votes(variants, gissue)

    max_variant_length = max((len(variant) for variant in variants))

    for variant, points in sorted(votes.items(), key=lambda i: i[1]):
        print ('%-*s %u' % (max_variant_length, variant, points))


if __name__ == '__main__':
    main()
